﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WebWinkel.Helpers
{
    public class Login
    {
        private static string _alias;
        private static string _password;
        private static int _attempts;
        private static DateTime _lastAttempt;
        private DataAccess _dal = new DataAccess();

        private string Encrypt(string data)
        {
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = GetMd5Hash(Encode(md5Hash, data));
                    return hash;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        private byte[] Encode(MD5 md5Hash, string input)
        {
            return md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
        }

        private static string GetMd5Hash(byte[] data)
        {
            StringBuilder sB = new StringBuilder();

            foreach (byte b in data)
            {
                sB.Append(b.ToString("x2"));
            }

            return sB.ToString();
        }

        private Models.LoginDetails FindData(bool secure)
        {
            try
            {
                if (secure)
                {
                    return _dal.ReadLogin(_alias, this.Encrypt(_password));
                }
                return _dal.ReadLogin(_alias);
            }
            catch (IndexOutOfRangeException)
            {
                throw;
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Models.LoginDetails SetDetails()
        {
            Models.LoginDetails data = new Models.LoginDetails();

            data.UserName = _alias;
            data.Password = Encrypt(_password);
            data.Attempts = 0;
            data.LastAttempt = DateTime.Now;

            return data;
        }

        private bool AddData(Models.LoginDetails data)
        {
            try
            {
                return _dal.InsertLogin(data);
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void FailedLogin()
        {
            Models.LoginDetails login = _dal.ReadLogin(_alias);
            if (login != null)
            {
                login.LastAttempt = _lastAttempt = DateTime.Now;
                login.Attempts = _attempts += 1;
                _dal.UpdateLogin(login);
            }
        }

        private void ClearLogin()
        {
            Models.LoginDetails login = _dal.ReadLogin(_alias, this.Encrypt(_password));
            if (login != null)
            {
                login.Attempts = _attempts = 0;
                _dal.UpdateLogin(login);
            }
        }

        public Login(string alias, string password)
        {
            _alias = alias;
            _password = password;
        }

        public bool Verify()
        {
            Models.LoginDetails data = FindData(true);

            if (data != null && data.Password == Encrypt(_password) && (data.Attempts < 5 || !this.Blocked()))
            {
                this.ClearLogin();
                return true;
            }

            FailedLogin();
            return false;
        }

        public bool Create()
        {
            if (_dal.ReadLogin(_alias) != null || _alias == null)
            {
                return false;
            }

            return AddData(SetDetails());
        }

        public bool check()
        {
            if (string.IsNullOrEmpty(_alias) || _dal.ReadLogin(_alias) != null)
            {
                return true;
            }
            return false;
        }

        public bool Blocked()
        {
            Models.LoginDetails data = FindData(false);
            if (data != null && data.Attempts > 4 && (DateTime.Now - data.LastAttempt).TotalMinutes < 10)
            {
                return true;
            }
            return false;
        }
    }
}