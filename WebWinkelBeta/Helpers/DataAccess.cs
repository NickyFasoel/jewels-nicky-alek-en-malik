﻿using System.Collections.Generic;

namespace WebWinkel.Helpers
{
    public class DataAccess
    {
        #region Read

        public List<Models.Postcodes> ReadAllPostcodes
        {
            get
            {
                using (Dal.IReadAllDB<Models.Postcodes> postcodes = new Dal.ReadPostcodes())
                {
                    return postcodes.ReadAll();
                }
            }
        }

        public List<Models.Knikkers> ReadAllKnikkers
        {
            get
            {
                using (Helpers.Dal.IReadAllDB<Models.Knikkers> knikkers = new Helpers.Dal.ReadKnikkers())
                {
                    return knikkers.ReadAll();
                }
            }
        }

        public Models.Orders GetLastOrder
        {
            get
            {
                using (Dal.IGetLastDB<Models.Orders> lastOrder = new Dal.GetLastOrder())
                {
                    return lastOrder.ReadOne();
                }
            }
        }

        public Models.Customers ReadCustomer(int id)
        {
            using (Dal.ReadData<Models.Customers> customer = new Dal.ReadCustomers(id))
            {
                return customer.ReadOne();
            }
        }

        public Models.Customers ReadCustomer(string id)
        {
            using (Dal.ReadData<Models.Customers> customer = new Dal.ReadCustomers(id))
            {
                return customer.ReadOne();
            }
        }

        public Models.Knikkers ReadKnikker(int id)
        {
            using (Dal.IReadDB<Models.Knikkers> knikker = new Dal.ReadKnikkers(id))
            {
                return knikker.ReadOne();
            }
        }

        public Models.LoginDetails ReadLogin(string id)
        {
            using (Dal.ReadData<Models.LoginDetails> login = new Dal.ReadLogin(id))
            {
                return login.ReadOne();
            }
        }

        public Models.LoginDetails ReadLogin(string id, string password)
        {
            using (Dal.ReadData<Models.LoginDetails> login = new Dal.ReadLogin(id, password))
            {
                return login.ReadOne();
            }
        }

        public Models.Postcodes ReadPostcode(int id)
        {
            using (Dal.IReadDB<Models.Postcodes> postcode = new Dal.ReadPostcodes(id))
            {
                return postcode.ReadOne();
            }
        }

        public Models.Sessies ReadSessie(Models.Sessies inputSessie)
        {
            using (Dal.IReadDB<Models.Sessies> outputSessie = new Dal.ReadSession(inputSessie))
            {
                return outputSessie.ReadOne();
            }
        }

        public Models.Sessies ReadSessie(int id)
        {
            using (Dal.IReadDB<Models.Sessies> sessie = new Dal.ReadSession(id))
            {
                return sessie.ReadOne();
            }
        }

        public List<Models.Sessies> ReadSessies(string id)
        {
            using (Dal.IReadAllDB<Models.Sessies> sessies = new Dal.ReadSession(id))
            {
                return sessies.ReadAll();
            }
        }

        public List<Models.Knikkers> SearchKnikkers(string searchString, SearchEnum column)
        {
            using (Dal.ISearchDB<Models.Knikkers> knikkers = new Dal.ReadKnikkers(searchString, column))
            {
                return knikkers.ReadAll();
            }
        }

        #endregion

        #region Insert

        public bool InsertOrder(Models.Orders order)
        {
            using (Dal.IInsertOneDB<Models.Orders> insertO = new Dal.InsertOrder(order))
            {
                return insertO.InsertOne();
            }
        }

        public bool InsertOrderDetails(Models.OrderDetails orderDetail)
        {
            using (Dal.IInsertOneDB<Models.OrderDetails> insertOD = new Dal.InsertOrderDetails(orderDetail))
            {
                return insertOD.InsertOne();
            }
        }

        public bool InsertCustomer(Models.Customers customer)
        {
            using (Dal.IInsertOneDB<Models.Customers> insertC = new Dal.InsertCustomer(customer))
            {
                return insertC.InsertOne();
            }
        }

        public bool InsertKnikker(Models.Knikkers knikker)
        {
            using (Dal.IInsertOneDB<Models.Knikkers> insertK = new Dal.InsertKnikker(knikker))
            {
                return insertK.InsertOne();
            }
        }

        public bool InsertLogin(Models.LoginDetails login)
        {
            using (Dal.IInsertOneDB<Models.LoginDetails> insertL = new Dal.InsertLogin(login))
            {
                return insertL.InsertOne();
            }
        }

        public bool InsertSession(Models.Sessies sessie)
        {
            using (Dal.IInsertOneDB<Models.Sessies> insertS = new Dal.InsertSession(sessie))
            {
                return insertS.InsertOne();
            }
        }

        #endregion

        #region Update

        public bool UpdateKnikker(Models.Knikkers knikker)
        {
            using (Dal.IUpdateOneDB<Models.Knikkers> updateK = new Dal.UpdateKnikker(knikker))
            {
                return updateK.UpdateOne();
            }
        }

        public bool UpdateLogin(Models.LoginDetails loginDetails)
        {
            using (Dal.IUpdateOneDB<Models.LoginDetails> updateLD = new Dal.UpdateLogin(loginDetails))
            {
                return updateLD.UpdateOne();
            }
        }

        public bool UpdateOrder(Models.Orders order)
        {
            using (Dal.IUpdateOneDB<Models.Orders> updateO = new Dal.UpdateOrder(order))
            {
                return updateO.UpdateOne();
            }
        }

        public bool UpdateSession(int id)
        {
            using (Dal.IUpdateOneDB<Models.Sessies> updateS = new Dal.UpdateSession(id))
            {
                return updateS.UpdateOne();
            }
        }

        #endregion

        #region Delete

        public bool DeleteKnikker(int id)
        {
            using (Dal.IDeleteFromDB<Models.Knikkers> knikker = new Dal.DeleteKnikker(id))
            {
                return knikker.DeleteOne();
            }
        }

        #endregion
    }
}