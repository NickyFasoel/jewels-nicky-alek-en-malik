﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace WebWinkel.Helpers
{
    public class Session
    {
        private DataAccess _dal = new DataAccess();

        private Models.Sessies CreateSession(string id)
        {
            Models.Sessies sessie = new Models.Sessies();

            sessie.Aangemaakt = DateTime.Now;
            sessie.Valid = true;
            sessie.LoginDetails = _dal.ReadLogin(id);

            if (AddSession(sessie))
            {
                OldSessions(sessie.SessieID = _dal.ReadSessie(sessie).SessieID, true);
                return sessie;
            }
            else
            {
                return null;
            }
        }

        private bool AddSession(Models.Sessies sessie)
        {
            try
            {
                return _dal.InsertSession(sessie);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string SessieString(Models.Sessies sessie)
        {
            return $"{sessie.SessieID}|{sessie.Aangemaakt.ToString()}|{sessie.Valid.ToString()}";
        }

        private void OldSessions(int id, bool current)
        {
            Models.Sessies details = _dal.ReadSessie(id);
            List<Models.Sessies> sessies = _dal.ReadSessies(details.LoginDetails.UserName);

            if (!current)
            {
                foreach (Models.Sessies sessie in sessies)
                {
                    this.UpdateSession(sessie.SessieID);
                }
            }
            else
            {
                foreach (Models.Sessies sessie in sessies.Where(s => s.SessieID != id))
                {
                    this.UpdateSession(sessie.SessieID);
                }
            }
        }

        private void UpdateSession(int id)
        {
            _dal.UpdateSession(id);
        }

        private Models.Sessies FindSession(string id)
        {
            return _dal.ReadSessie(int.Parse(id));
        }

        public string Sessie(string id)
        {
            Models.Sessies sessie = CreateSession(id);
            if (sessie.Valid == true)
            {
                return SessieString(sessie);
            }
            return null;
        }

        public bool Verify(string[] cookie)
        {
            if (cookie[0] != "false")
            {
                Models.Sessies sessie = FindSession(cookie[0]);

                if (sessie != null && sessie.Aangemaakt.ToString().Equals(cookie[1]) && sessie.Valid == true)
                {
                    return true;
                }
            }
            return false;
        }

        public void Logout(int id)
        {
            this.OldSessions(id, false);
        }

        public string User(string[] cookie)
        {
            return FindSession(cookie[0]).LoginDetails.UserName;
        }
    }
}