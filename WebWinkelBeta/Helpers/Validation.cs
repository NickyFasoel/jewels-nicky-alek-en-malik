﻿using System.ComponentModel.DataAnnotations;
using System;

namespace WebWinkel.Helpers
{
    [AttributeUsage(AttributeTargets.Class)]
    public class UsernameTakenAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (Register)value;

            if (new Login(model.UserName, null).check())
            {
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class WrongDataAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (Models.LoginDetails)value;
            Login login = new Login(model.UserName, model.Password);
            
            if (model.LastAttempt == DateTime.MinValue && login.Blocked())
            {
                this.ErrorMessage = "this username is currently blocked, please wait 10 minutes before trying again.";
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            if (model.LastAttempt == DateTime.MinValue && !login.Verify())
            {
                this.ErrorMessage = "This combination of username and password does not exist.";
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }
    }
}