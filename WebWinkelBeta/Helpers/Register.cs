﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Helpers
{
    [UsernameTaken(ErrorMessage = "This username is already taken, please type a new one.")]
    public class Register
    {
        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(25, ErrorMessage = "{0} only allows a max of 25 characters long.")]
        [DisplayName("Name")]
        public string Voornaam { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        [DisplayName("Surname")]
        public string Familienaam { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(200, ErrorMessage = "{0} only allows a max of 200 characters long.")]
        [DisplayName("Street")]
        public string Straat { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(6, ErrorMessage = "{0} only allows a max of 6 characters long.")]
        [Column(TypeName = "varchar")]
        [DataType("int")]
        [DisplayName("Number")]
        public string Huisnummer { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        [DisplayName("Country")]
        public string Land { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(75, ErrorMessage = "{0} only allows a max of 75 characters long.")]
		[EmailAddress(ErrorMessage = "Invalid Email Address.")]
		[DisplayName("E-mail")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(150, ErrorMessage = "{0} only allows a max of 150 characters long.")]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "{0} has to be atleast 6 characters long.")]
		[DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        public int PostcodeID { get; set; }
    }
}