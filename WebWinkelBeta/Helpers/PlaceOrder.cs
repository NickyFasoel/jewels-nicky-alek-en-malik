﻿using System.Collections.Generic;

namespace WebWinkel.Helpers
{
    public class PlaceOrder
    {
        private static Models.Orders _order;
        private List<Models.OrderDetails> _details;
        private static decimal _total;
        private DataAccess _dal = new DataAccess();

        private bool SetOrderDetailsAndTotal(Models.Orders id)
        {
            foreach (Models.OrderDetails od in this._details)
            {
                od.OrderID = id;
                _total += (od.ProductID.Prijs * od.Aantal);

                if (!_dal.InsertOrderDetails(od))
                {
                    return false;
                }
            }
            return true;
        }

        public PlaceOrder(Models.Orders order, List<Models.OrderDetails> details)
        {
            _order = order;
            _details = details;
            _total = 0;
        }

        public bool ExecuteOrder()
        {
            if (_dal.InsertOrder(_order))
            {
                Models.Orders order = _dal.GetLastOrder;

                order.Aantal = _details.Count;

                if (!this.SetOrderDetailsAndTotal(order))
                {
                    return false;
                }

                order.TotaalPrijs = _total;

                return _dal.UpdateOrder(order);
            }

            return false;
        }
    }
}