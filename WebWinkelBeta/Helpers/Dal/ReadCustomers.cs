﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class ReadCustomers : ReadData<Models.Customers>, IReadDB<Models.Customers>
    {
        private static string _params;
        private DataAccess _dal = new DataAccess();

        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.ReadOne)
            {
                int id;
                if (int.TryParse(_params, out id))
                {
                    this._command.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = id;
                }
                else
                {
                    this._command.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar, 250)).Value = _params;
                }
            }
        }

        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.Customers();
                this._modelData.ID = (int)this._result["ID"];
                this._modelData.Voornaam = this._result["Voornaam"].ToString();
                this._modelData.Familienaam = this._result["Familienaam"].ToString();
                this._modelData.Huisnummer = this._result["Huisnummer"].ToString();
                this._modelData.Straat = this._result["Straat"].ToString();
                this._modelData.Land = this._result["Land"].ToString();
                this._modelData.Email = this._result["Email"].ToString();
                this._modelData.LoginData = _dal.ReadLogin(this._result["LoginData_UserName"].ToString());
                this._modelData.Postcodes = _dal.ReadPostcode((int)this._result["Postcodes_ID"]);
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public ReadCustomers(int id)
        {
            this._sqlString = "ReadOneCustomer";
            _params = id.ToString();
            this.CommandAttributes(Procedures.ReadOne);
        }

        public ReadCustomers(string id)
        {
            this._sqlString = "ReadOneCustomerFromUserName";
            _params = id;
            this.CommandAttributes(Procedures.ReadOne);
        }

        public override void Dispose()
        {
            _params = null;
            _dal = null;
            base.Dispose();
        }
    }
}