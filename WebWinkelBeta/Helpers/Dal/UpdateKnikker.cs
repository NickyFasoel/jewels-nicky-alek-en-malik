﻿using System.Data.SqlClient;
using System.Data;

namespace WebWinkel.Helpers.Dal
{
    public class UpdateKnikker : UpdateData<Models.Knikkers>, IUpdateOneDB<Models.Knikkers>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.UpdateOne)
            {
                this._command.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = this._modelData.ID;
                this._command.Parameters.Add(new SqlParameter("@Naam", SqlDbType.NVarChar, 50)).Value = this._modelData.Naam;
                this._command.Parameters.Add(new SqlParameter("@Grootte", SqlDbType.Float)).Value = this._modelData.Grootte;
                this._command.Parameters.Add(new SqlParameter("@Kleur", SqlDbType.NVarChar, 75)).Value = this._modelData.Kleur;
                this._command.Parameters.Add(new SqlParameter("@Prijs", SqlDbType.Money)).Value = this._modelData.Prijs;
                this._command.Parameters.Add(new SqlParameter("@Materiaal", SqlDbType.NVarChar, 75)).Value = this._modelData.Materiaal;
                this._command.Parameters.Add(new SqlParameter("@Foto", SqlDbType.NVarChar, 250)).Value = this._modelData.Foto;
            }
        }

        public UpdateKnikker(Models.Knikkers knikker)
        {
            this._sqlString = "UpdateOneKnikker";
            this._modelData = knikker;
            this.CommandAttributes(Procedures.UpdateOne);
        }
    }
}