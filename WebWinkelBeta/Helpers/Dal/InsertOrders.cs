﻿using System.Data.SqlClient;
using System.Data;

namespace WebWinkel.Helpers.Dal
{
    public class InsertOrder : InsertData<Models.Orders>, IInsertOneDB<Models.Orders>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Aantal", SqlDbType.Int)).Value = this._modelData.Aantal;
                this._command.Parameters.Add(new SqlParameter("@BestelDatum", SqlDbType.Date)).Value = this._modelData.BestelDatum;
                this._command.Parameters.Add(new SqlParameter("@TotaalPrijs", SqlDbType.Money)).Value = this._modelData.TotaalPrijs;
                this._command.Parameters.Add(new SqlParameter("@Betaald", SqlDbType.Bit)).Value = this._modelData.Betaald;
                this._command.Parameters.Add(new SqlParameter("@Retour", SqlDbType.Int)).Value = this._modelData.Aantal;
                this._command.Parameters.Add(new SqlParameter("@KlantID", SqlDbType.Int)).Value = this._modelData.KlantID.ID;
            }
        }

        public InsertOrder(Models.Orders order)
        {
            this._modelData = order;
            this._sqlString = "InsertOrder";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}