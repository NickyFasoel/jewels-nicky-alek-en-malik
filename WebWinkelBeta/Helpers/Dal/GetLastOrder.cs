﻿using System;

namespace WebWinkel.Helpers.Dal
{
    public class GetLastOrder : GetLastRowData<Models.Orders>, IGetLastDB<Models.Orders>
    {
        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.Orders();
                this._modelData.ID = (int)this._result["ID"];
                this._modelData.Aantal = (int)this._result["Aantal"];
                this._modelData.BestelDatum = (DateTime)this._result["BestelDatum"];
                this._modelData.TotaalPrijs = (decimal)this._result["TotaalPrijs"];
                this._modelData.Betaald = (bool)this._result["Betaald"];
                this._modelData.Retour = (bool)this._result["Retour"];
                this._modelData.KlantID = new ReadCustomers((int)this._result["KlantID_ID"]).ReadOne();
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.FindLast)
            {
                this._sqlString = "FindLastOrder";
            }
        }

        public GetLastOrder()
        {
            this.CommandAttributes(Procedures.FindLast);
        }
    }
}