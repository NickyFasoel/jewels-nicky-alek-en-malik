﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class ReadLogin : ReadData<Models.LoginDetails> , IReadDB<Models.LoginDetails>
    {
        private static string[] _params;

        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.ReadOne)
            {
                this._command.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar, 250)).Value = _params[0];

                if (_params.Length == 2)
                {
                    this._command.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar, 250)).Value = _params[1];
                }
            }
        }

        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.LoginDetails();
                this._modelData.UserName = this._result["UserName"].ToString();
                this._modelData.Attempts = (int)this._result["Attempts"];
                this._modelData.LastAttempt = (DateTime)this._result["LastAttempt"];
                if (_params.Length == 2)
                {
                    this._modelData.Password = this._result["Password"].ToString();
                }
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public ReadLogin(string id)
        {
            this._sqlString = "CheckLoginData";
            _params = new string[1] { id };
            this.CommandAttributes(Procedures.ReadOne);
        }

        public ReadLogin(string id, string password)
        {
            this._sqlString = "ValidateLoginData";
            _params = new string[2] { id, password };
            this.CommandAttributes(Procedures.ReadOne);
        }

        public override void Dispose()
        {
            _params = null;
            base.Dispose();
        }
    }
}