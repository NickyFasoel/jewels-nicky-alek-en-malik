﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class UpdateSession : UpdateData<Models.Sessies> , IUpdateOneDB<Models.Sessies>
    {
        private int _id;

        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.UpdateOne)
            {
                this._command.Parameters.Add(new SqlParameter("@SessieID", SqlDbType.Int)).Value = this._id;
            }
        }

        public UpdateSession(int id)
        {
            this._sqlString = "UpdateSession";
            this._id = id;
            this.CommandAttributes(Procedures.UpdateOne);
        }
    }
}