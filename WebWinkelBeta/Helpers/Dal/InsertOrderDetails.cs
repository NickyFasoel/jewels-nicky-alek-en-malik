﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class InsertOrderDetails : InsertData<Models.OrderDetails>, IInsertOneDB<Models.OrderDetails>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@VerstuurDatum", SqlDbType.Date)).Value = this._modelData.VerstuurDatum;
                this._command.Parameters.Add(new SqlParameter("@RetourDatum", SqlDbType.Date)).Value = this._modelData.RetourDatum;
                this._command.Parameters.Add(new SqlParameter("@Aantal", SqlDbType.Int)).Value = this._modelData.Aantal;
                this._command.Parameters.Add(new SqlParameter("@OrderID", SqlDbType.Int)).Value = this._modelData.OrderID.ID;
                this._command.Parameters.Add(new SqlParameter("@ProductID", SqlDbType.Int)).Value = this._modelData.ProductID.ID;
            }
        }

        public InsertOrderDetails(Models.OrderDetails orderDetails)
        {
            this._modelData = orderDetails;
            this._sqlString = "InsertOrderDetails";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}