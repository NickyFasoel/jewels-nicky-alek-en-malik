﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class InsertKnikker : InsertData<Models.Knikkers>, IInsertOneDB<Models.Knikkers>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Naam", SqlDbType.NVarChar, 75)).Value = this._modelData.Naam;
                this._command.Parameters.Add(new SqlParameter("@Grootte", SqlDbType.Float)).Value = this._modelData.Grootte;
                this._command.Parameters.Add(new SqlParameter("@Kleur", SqlDbType.NVarChar, 75)).Value = this._modelData.Kleur;
                this._command.Parameters.Add(new SqlParameter("@Materiaal", SqlDbType.NVarChar, 75)).Value = this._modelData.Materiaal;
                this._command.Parameters.Add(new SqlParameter("@Prijs", SqlDbType.Money)).Value = this._modelData.Prijs;
                this._command.Parameters.Add(new SqlParameter("@Foto", SqlDbType.NVarChar, 250)).Value = this._modelData.Foto;
            }
        }

        public InsertKnikker(Models.Knikkers knikker)
        {
            this._modelData = knikker;
            this._sqlString = "InsertKnikker";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}