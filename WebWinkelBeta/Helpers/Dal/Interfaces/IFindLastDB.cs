﻿using System;

namespace WebWinkel.Helpers.Dal
{
    interface IGetLastDB<T> : IReadDB<T>, IDisposable
    {
        T FindLast();
    }
}
