﻿using System;

namespace WebWinkel.Helpers.Dal
{
    interface IInsertOneDB<T> : IDisposable
    {
        bool InsertOne();
    }
}
