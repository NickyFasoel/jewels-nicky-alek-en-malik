﻿using System;

namespace WebWinkel.Helpers.Dal
{
    interface IReadDB<T> : IDisposable
    {
        T ReadOne();
    }
}
