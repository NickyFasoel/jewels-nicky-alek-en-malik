﻿using System.Collections.Generic;
using System;

namespace WebWinkel.Helpers.Dal
{
    interface IReadAllDB<T> : IReadDB<T>, IDisposable
    {
        List<T> ReadAll();
    }
}
