﻿using System;

namespace WebWinkel.Helpers.Dal
{
    interface IUpdateOneDB<T>: IDisposable
    {
        bool UpdateOne();
    }
}
