﻿using System.Collections.Generic;
using System;

namespace WebWinkel.Helpers.Dal
{
    interface ISearchDB<T> : IReadAllDB<T>, IDisposable
    {
        List<T> Search();
    }
}
