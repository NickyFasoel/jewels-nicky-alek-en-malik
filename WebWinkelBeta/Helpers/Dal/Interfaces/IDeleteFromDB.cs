﻿using System;

namespace WebWinkel.Helpers.Dal
{
    interface IDeleteFromDB<T> : IDisposable
    {
        bool DeleteOne();
    }
}
