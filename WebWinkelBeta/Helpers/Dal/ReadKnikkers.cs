﻿using System;
using System.Data.SqlClient;
using System.Data;

public enum SearchEnum { Search, Naam, Kleur }

namespace WebWinkel.Helpers.Dal
{
    public class ReadKnikkers : SearchData<Models.Knikkers>, IReadAllDB<Models.Knikkers>, IReadDB<Models.Knikkers>, ISearchDB<Models.Knikkers>
    {
        private static string _params;
        private static SearchEnum _search;

        private void SearchSwitch()
        {
            switch (_search)
            {
                case SearchEnum.Naam:
                    {
                        this._command.Parameters.Add(new SqlParameter("@Naam", SqlDbType.Bit)).Value = true;
                        this._command.Parameters.Add(new SqlParameter("@Kleur", SqlDbType.Bit)).Value = false;
                    }
                    break;
                case SearchEnum.Kleur:
                    {
                        this._command.Parameters.Add(new SqlParameter("@Naam", SqlDbType.Bit)).Value = false;
                        this._command.Parameters.Add(new SqlParameter("@Kleur", SqlDbType.Bit)).Value = true;
                    }
                    break;
                default:
                    {
                        this._command.Parameters.Add(new SqlParameter("@Naam", SqlDbType.Bit)).Value = false;
                        this._command.Parameters.Add(new SqlParameter("@Kleur", SqlDbType.Bit)).Value = false;
                    }
                    break;
            }
        }

        protected override void CommandAttributes(Procedures procedure)
        {
            switch (procedure)
            {
                case Procedures.ReadOne:
                    {
                        this._command.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = Convert.ToInt32(_params);
                    }
                    break;
                case Procedures.Search:
                    {
						this.SearchSwitch();
                        this._command.Parameters.Add(new SqlParameter("@SearchString", SqlDbType.NVarChar, 75)).Value = _params;
                    }
                    break;
            }
        }

        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.Knikkers();
                this._modelData.ID = (int)this._result["ID"];
                this._modelData.Naam = this._result["Naam"].ToString();
                this._modelData.Kleur = this._result["Kleur"].ToString();
                this._modelData.Materiaal = this._result["Materiaal"].ToString();
                this._modelData.Grootte = (float)(double)this._result["Grootte"];
                this._modelData.Prijs = (decimal)this._result["Prijs"];
                this._modelData.Foto = this._result["Foto"].ToString();
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public ReadKnikkers()
        {
            this._sqlString = "ReadAllKnikkers";
        }

        public ReadKnikkers(int id)
        {
            this._sqlString = "ReadOneKnikker";
            _params = id.ToString();
            this.CommandAttributes(Procedures.ReadOne);
        }

        public ReadKnikkers(string SearchString, SearchEnum column)
        {
            this._sqlString = "SearchKnikker";
            _params = SearchString;
            _search = column;
            this.CommandAttributes(Procedures.Search);
        }

        public override void Dispose()
        {
            _params = null;
            base.Dispose();
        }
    }
}