﻿using System.Data.SqlClient;
using System.Data;

namespace WebWinkel.Helpers.Dal
{
    public class InsertLogin : InsertData<Models.LoginDetails> , IInsertOneDB<Models.LoginDetails>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar)).Value = this._modelData.UserName;
                this._command.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar)).Value = this._modelData.Password;
            }
        }

        public InsertLogin(Models.LoginDetails loginData)
        {
            this._modelData = loginData;
            this._sqlString = "InsertLoginDetails";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}