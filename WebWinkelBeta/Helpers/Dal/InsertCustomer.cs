﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class InsertCustomer : InsertData<Models.Customers> , IInsertOneDB<Models.Customers>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Voornaam", SqlDbType.NVarChar, 25)).Value = this._modelData.Voornaam;
                this._command.Parameters.Add(new SqlParameter("@Familienaam", SqlDbType.NVarChar, 50)).Value = this._modelData.Familienaam;
                this._command.Parameters.Add(new SqlParameter("@Straat", SqlDbType.NVarChar, 200)).Value = this._modelData.Straat;
                this._command.Parameters.Add(new SqlParameter("@Huisnummer", SqlDbType.VarChar, 6)).Value = this._modelData.Huisnummer;
                this._command.Parameters.Add(new SqlParameter("@Land", SqlDbType.NVarChar, 50)).Value = this._modelData.Land;
                this._command.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar, 75)).Value = this._modelData.Email;
                this._command.Parameters.Add(new SqlParameter("@LoginData", SqlDbType.NVarChar, 250)).Value = this._modelData.LoginData.UserName;
                this._command.Parameters.Add(new SqlParameter("@PostcodeID", SqlDbType.Int)).Value = this._modelData.Postcodes.ID;
            }
        }

        public InsertCustomer(Models.Customers customer)
        {
            this._modelData = customer;
            this._sqlString = "InsertOneCustomer";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}