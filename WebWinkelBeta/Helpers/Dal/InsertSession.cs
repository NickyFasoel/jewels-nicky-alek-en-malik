﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class InsertSession : InsertData<Models.Sessies> , IInsertOneDB<Models.Sessies>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.InsertOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Aangemaakt", SqlDbType.DateTime)).Value = this._modelData.Aangemaakt;
                this._command.Parameters.Add(new SqlParameter("@LoginData", SqlDbType.NVarChar, 250)).Value = this._modelData.LoginDetails.UserName;
            }
        }

        public InsertSession(Models.Sessies sessie)
        {
            this._modelData = sessie;
            this._sqlString = "InsertOneSession";
            this.CommandAttributes(Procedures.InsertOne);
        }
    }
}