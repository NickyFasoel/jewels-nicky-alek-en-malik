﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class ReadPostcodes : ReadAllData<Models.Postcodes> , IReadDB<Models.Postcodes> , IReadAllDB<Models.Postcodes>
    {
        private static int _params;

        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.ReadOne)
            {
                this._command.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = _params;
            }
        }

        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.Postcodes();
                this._modelData.ID = (int)this._result["ID"];
                this._modelData.Postcode = this._result["Postcode"].ToString();
                this._modelData.Stad = this._result["Stad"].ToString();
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public ReadPostcodes()
        {
            this._sqlString = "ReadAllPostcodes";
            this.CommandAttributes(Procedures.ReadAll);
        }

        public ReadPostcodes(int id)
        {
            this._sqlString = "ReadOnePostcode";
            _params = id;
            this.CommandAttributes(Procedures.ReadOne);
        }
    }
}