﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class UpdateLogin : UpdateData<Models.LoginDetails> , IUpdateOneDB<Models.LoginDetails>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.UpdateOne)
            {
                this._command.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar, 250)).Value = this._modelData.UserName;
                this._command.Parameters.Add(new SqlParameter("@Attempts", SqlDbType.Int)).Value = this._modelData.Attempts;
                this._command.Parameters.Add(new SqlParameter("@LastAttempt", SqlDbType.DateTime)).Value = this._modelData.LastAttempt;
            }
        }

        public UpdateLogin(Models.LoginDetails login)
        {
            this._sqlString = "UpdateLoginState";
            this._modelData = login;
            this.CommandAttributes(Procedures.UpdateOne);
        }
    }
}