﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class ReadSession : ReadAllData<Models.Sessies> , IReadDB<Models.Sessies>, IReadAllDB<Models.Sessies>
    {
        private string _id;

        protected override void CommandAttributes(Procedures procedure)
        {
            switch (procedure)
            {
                case Procedures.ReadOne:
                    {
                        if (this._sqlString == "ReadSession")
                        {
                            this._command.Parameters.Add(new SqlParameter("@Aangemaakt", SqlDbType.DateTime)).Value = this._modelData.Aangemaakt;
                            this._command.Parameters.Add(new SqlParameter("@Valid", SqlDbType.Bit)).Value = this._modelData.Valid;
                            this._command.Parameters.Add(new SqlParameter("@LoginData", SqlDbType.NVarChar, 250)).Value = this._modelData.LoginDetails.UserName;
                        }
                        else
                        {
                            this._command.Parameters.Add(new SqlParameter("@SessieID", SqlDbType.Int)).Value = this._modelData.SessieID;
                        }
                    }
                    break;
                case Procedures.ReadAll:
                    {
                        this._command.Parameters.Add(new SqlParameter("@LoginData", SqlDbType.NVarChar, 250)).Value = this._id;
                    }
                    break;
            }
        }

        protected override bool Read()
        {
            try
            {
                this._modelData = new Models.Sessies();
                this._modelData.SessieID = (int)this._result["SessieID"];
                this._modelData.Aangemaakt = (DateTime)this._result["Aangemaakt"];
                this._modelData.Valid = (bool)this._result["Valid"];
                this._modelData.LoginDetails = new ReadLogin(this._result["LoginDetails_UserName"].ToString()).ReadOne();
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public ReadSession(Models.Sessies sessie)
        {
            this._sqlString = "ReadSession";
            this._modelData = sessie;
            this.CommandAttributes(Procedures.ReadOne);
        }


        public ReadSession(int id)
        {
            this._sqlString = "ReadSessionFromId";
            this._modelData = new Models.Sessies();
            this._modelData.SessieID = id;
            this.CommandAttributes(Procedures.ReadOne);
        }

        public ReadSession(string id)
        {
            this._sqlString = "ReadSomeSessions";
            this._id = id;
            this.CommandAttributes(Procedures.ReadAll);
        }

        public override void Dispose()
        {
            _id = null;
            base.Dispose();
        }
    }
}