﻿using System.Data;

namespace WebWinkel.Helpers.Dal
{
    public class DeleteKnikker : DeleteData<Models.Knikkers>, IDeleteFromDB<Models.Knikkers>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.DeleteOne)
            {
                this._command.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ID", SqlDbType.Int)).Value = this._modelId;
            }
        }

        public DeleteKnikker(int id)
        {
            this._sqlString = "DeleteOneKnikker";
            this._modelId = id;
            this.CommandAttributes(Procedures.DeleteOne);
        }
    }
}