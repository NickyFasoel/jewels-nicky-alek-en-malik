﻿namespace WebWinkel.Helpers.Dal
{
    public abstract class ReadData<T> : DatabaseInteraction , IReadDB<T>
    {
        protected T _modelData;

        protected override bool ConnectionSwitch()
        {
            if (_currentAction == Procedures.ReadOne)
            {
                this._result.Read();
                return this.Read();
            }
            return false;
        }

        protected abstract bool Read();

        public virtual T ReadOne()
        {
            _currentAction = Procedures.ReadOne;
            if (this.Connect())
            {
                return this._modelData;
            }
            return default(T);
        }

        public override void Dispose()
        {
            this._modelData = default(T);
            base.Dispose();
        }
    }
}