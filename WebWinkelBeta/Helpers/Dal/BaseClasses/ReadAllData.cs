﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebWinkel.Helpers.Dal
{
    public abstract class ReadAllData<T> : ReadData<T> ,IReadDB<T>,IReadAllDB<T>
    {
        protected List<T> _ModelDataList;

        protected bool ReadAllModelData()
        {
            this._ModelDataList = new List<T>();
            while (this._result.Read())
            {
                if (this.Read())
                {
                    this._ModelDataList.Add(this._modelData);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        protected override bool ConnectionSwitch()
        {
            switch (_currentAction)
            {
                case Procedures.ReadOne:
                    {
                        this._result.Read();
                        return this.Read();
                    }
                case Procedures.ReadAll:
                    {
                        return this.ReadAllModelData();
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        public virtual List<T> ReadAll()
        {
            _currentAction = Procedures.ReadAll;
            if (this.Connect())
            {
                return this._ModelDataList;
            }
            return null;
        }

        public override void Dispose()
        {
            this._ModelDataList = null;
            base.Dispose();
        }
    }
}