﻿namespace WebWinkel.Helpers.Dal
{
    public abstract class InsertData<T> : AlterData<T> , IInsertOneDB<T>
    {
        protected T _modelData;

        public bool InsertOne()
        {
            _currentAction = Procedures.InsertOne;
            return this.Connect();
        }

        public override void Dispose()
        {
            this._modelData = default(T);
            base.Dispose();
        }
    }
}