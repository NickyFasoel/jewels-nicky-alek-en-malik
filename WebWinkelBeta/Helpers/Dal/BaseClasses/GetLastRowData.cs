﻿namespace WebWinkel.Helpers.Dal
{
    public abstract class GetLastRowData<T> : ReadData<T>, IReadDB<T>, IGetLastDB<T>
    {
        protected override bool ConnectionSwitch()
        {
            if (_currentAction == Procedures.FindLast || _currentAction == Procedures.ReadOne)
            {
                this._result.Read();
                return this.Read();
            }
            return false;
        }
        
        public T FindLast()
        {
            _currentAction = Procedures.FindLast;
            if (this.Connect())
            {
                return this._modelData;
            }
            return default(T);
        }
    }
}