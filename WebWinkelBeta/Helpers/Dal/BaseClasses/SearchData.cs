﻿using System.Collections.Generic;

namespace WebWinkel.Helpers.Dal
{
    public abstract class SearchData<T> : ReadAllData<T>, ISearchDB<T>
    {
        protected override bool ConnectionSwitch()
        {
            switch (_currentAction)
            {
                case Procedures.ReadOne:
                    {
                        this._result.Read();
                        return this.Read();
                    }
                case Procedures.ReadAll:
                case Procedures.Search:
                    {
                        return this.ReadAllModelData();
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        public List<T> Search()
        {
            _currentAction = Procedures.Search;
            this._command.CommandText = this._sqlString;
            if (this.Connect())
            {
                return this._ModelDataList;
            }
            return null;
        }
    }
}