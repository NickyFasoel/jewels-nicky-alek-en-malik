﻿namespace WebWinkel.Helpers.Dal
{
    public enum Procedures : int { ReadOne = 1, ReadAll, Search, FindLast, InsertOne, UpdateOne, DeleteOne }
}