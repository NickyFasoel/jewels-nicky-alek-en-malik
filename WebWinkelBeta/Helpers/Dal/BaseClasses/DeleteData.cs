﻿namespace WebWinkel.Helpers.Dal
{
    public abstract class DeleteData<T> : AlterData<T>, IDeleteFromDB<T>
    {
        protected int _modelId;

        public bool DeleteOne()
        {
            _currentAction = Procedures.DeleteOne;
            return this.Connect();
        }

        public override void Dispose()
        {
            this._modelId = 0;
            base.Dispose();
        }
    }
}