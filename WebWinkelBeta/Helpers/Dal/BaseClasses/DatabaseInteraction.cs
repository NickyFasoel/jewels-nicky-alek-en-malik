﻿using System;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public abstract class DatabaseInteraction : IDisposable
    {
        protected SqlConnection _connection;
        protected SqlCommand _command;
        protected string _sqlString;
        protected SqlDataReader _result;
        protected Procedures _currentAction;

        protected bool Connect()
        {
            this._command.CommandText = this._sqlString;
            using (_connection)
            {
                try
                {
                    _connection.Open();
                }
                catch (SqlException)
                {
                    return false;
                }
                this.SetResult();
                _connection.Close();
                this._command.Parameters.Clear();
            }
            return true;
        }

        protected bool SetResult()
        {
            if ((int)this._currentAction <= 4)
            {
                using (this._result = this._command.ExecuteReader())
                {
                    if (this._result.HasRows)
                    {
                        return this.ConnectionSwitch();
                    }
                }
            }
            else
            {
                return this.ConnectionSwitch();
            }

            return false;
        }

        protected abstract bool ConnectionSwitch();

        protected abstract void CommandAttributes(Procedures procedure);

        public DatabaseInteraction()
        {
            Connection connect = Connection.GetConnection;
            this._command = new SqlCommand();
            this._command.Connection = this._connection = connect.SqlConnection;
            this._command.CommandType = System.Data.CommandType.StoredProcedure;
        }

        public virtual void Dispose()
        {
            this._command = null;
            this._sqlString = null;
            this._result = null;
        }

    }
}