﻿using System.Data.SqlClient;
using System.Configuration;

namespace WebWinkel.Helpers.Dal
{
    public sealed class Connection
    {
        private static Connection _connection;
        private static SqlConnection _sqlConnection;

        private void SetConnectionString()
        {
            _sqlConnection = new SqlConnection();
            _sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Jewels"].ToString();
        }

        private Connection()
        {
        }
        
        public SqlConnection SqlConnection
        {
            get
            {
                return _sqlConnection;
            }
        }
        
        public static Connection GetConnection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new Connection();
                }
                _connection.SetConnectionString();
                return _connection;
            }
            
        }
    }
}