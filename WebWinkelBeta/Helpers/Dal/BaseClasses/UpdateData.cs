﻿namespace WebWinkel.Helpers.Dal
{
    public abstract class UpdateData<T> : AlterData<T>, IUpdateOneDB<T>
    {
        protected T _modelData;

        public bool UpdateOne()
        {
            _currentAction = Procedures.UpdateOne;
            return this.Connect();
        }

        public override void Dispose()
        {
            this._modelData = default(T);
            base.Dispose();
        }
    }
}