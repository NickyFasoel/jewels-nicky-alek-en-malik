﻿using System;

namespace WebWinkel.Helpers.Dal
{
    public abstract class AlterData<T> : DatabaseInteraction
    {
        protected int _intResult;

        protected override bool ConnectionSwitch()
        {
            switch (_currentAction)
            {
                case Procedures.InsertOne:
                case Procedures.DeleteOne:
                case Procedures.UpdateOne:
                    {
                        return this.Execute();
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        protected bool Execute()
        {
            try
            {
                this._intResult = this._command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}