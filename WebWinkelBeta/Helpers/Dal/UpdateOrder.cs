﻿using System.Data;
using System.Data.SqlClient;

namespace WebWinkel.Helpers.Dal
{
    public class UpdateOrder : UpdateData<Models.Orders> , IUpdateOneDB<Models.Orders>
    {
        protected override void CommandAttributes(Procedures procedure)
        {
            if (procedure == Procedures.UpdateOne)
            {
                this._command.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = this._modelData.ID;
                this._command.Parameters.Add(new SqlParameter("@Aantal", SqlDbType.Int)).Value = this._modelData.Aantal;
                this._command.Parameters.Add(new SqlParameter("@TotaalPrijs", SqlDbType.Money)).Value = this._modelData.TotaalPrijs;
            }
        }

        public UpdateOrder(Models.Orders order)
        {
            this._sqlString = "UpdateOrder";
            this._modelData = order;
            this.CommandAttributes(Procedures.UpdateOne);
        }
    }
}