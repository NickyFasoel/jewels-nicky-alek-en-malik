﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebWinkel.Helpers
{
    public class Cookies
    {
        private Session _sessie = new Session();

        private HttpCookie SetTime(HttpCookie cookie)
        {
            cookie.Expires = DateTime.Now.AddMinutes(30);
            return cookie;
        }

        private string[] DisectCookie(string waarde)
        {
            return waarde.Split('|');
        }

        private int CookieID(string[] cookie)
        {
            int id = 0;
            int.TryParse(cookie[0], out id);
            return id;
        }

        public HttpCookie LoginCookie(string username)
        {
            var cookie = new HttpCookie("KnikkersStore", _sessie.Sessie(username));
            return SetTime(cookie);
        }

        public bool Verify(HttpCookie cookie)
        {
            if (cookie == null)
            {
                return false;
            }
            if (_sessie.Verify(DisectCookie(cookie.Value.ToString())))
            {
                return true;
            }
            return false;
        }

        public bool VerifyData(string cookie)
        {
            if (cookie == null)
            {
                return false;
            }
            if (_sessie.Verify(DisectCookie(cookie)))
            {
                return true;
            }
            return false;
        }

        public HttpCookie Logout(HttpCookie cookie)
        {
            _sessie.Logout(CookieID(DisectCookie(cookie.Value)));
            return new HttpCookie("false");
        }

        public string User(string cookie)
        {
            if (this.VerifyData(cookie))
            {
                return _sessie.User(this.DisectCookie(cookie));
            }
            return null;
        }

        //malik

        public Models.Customers FindId(string cookie)
        {
            string user = this.User(cookie);

            return new Dal.ReadCustomers(user).ReadOne();
        }

        public void RemoveId(HttpCookie cookie, string id)
        {
            Dictionary<string, string> tempdict = CookieToDictionary(cookie.Value);

            tempdict.Remove(id);

            cookie.Value = DictionaryToCookie(tempdict);

        }
        
        public static Dictionary<string, string> CookieToDictionary(string cookie)
        {
            string[] temparr = cookie.Split('|');
            temparr = temparr.Reverse().Skip(1).Reverse().ToArray();

            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (string item in temparr)
            {
                string[] temparr2 = item.Split(' ');
                dict.Add(temparr2[0], temparr2[1]);
            }

            return dict;
        }

        public static string DictionaryToCookie(Dictionary<string, string> dict)
        {
            string temp = "";
            foreach (KeyValuePair<string, string> item in dict)
            {
                temp += item.Key + " " + item.Value + "|";
            }
            return temp;
        }
    }
}