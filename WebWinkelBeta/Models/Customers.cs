﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Customers
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(25, ErrorMessage = "{0} only allows a max of 25 characters long.")]
        public string Voornaam { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        public string Familienaam { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(200, ErrorMessage = "{0} only allows a max of 200 characters long.")]
        public string Straat { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(6, ErrorMessage = "{0} only allows a max of 6 characters long.")]
        [Column(TypeName = "varchar")]
        public string Huisnummer { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        public string Land { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
		[DataType(DataType.EmailAddress, ErrorMessage = "E-mail must a valid e-mail address")]
        [MaxLength(75, ErrorMessage = "{0} only allows a max of 75 characters long.")]
        public string Email { get; set; }

        public virtual LoginDetails LoginData { get; set; }
        public virtual Postcodes Postcodes { get; set; }
    }
}