﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class OrderDetails
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime VerstuurDatum { get; set; }

        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime RetourDatum { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        public int Aantal { get; set; }
        
        public virtual Orders OrderID { get; set; }
        public virtual Knikkers ProductID { get; set; }
    }
}