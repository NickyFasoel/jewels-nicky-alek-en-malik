﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Knikkers
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        [DisplayName("Name")]
        public string Naam { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "float")]
        [DisplayName("Size")]
        public float Grootte { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(75, ErrorMessage = "{0} only allows a max of 75 characters long.")]
        [DisplayName("Color")]
        public string Kleur { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(75, ErrorMessage = "{0} only allows a max of 75 characters long.")]
        [DisplayName("Material")]
        public string Materiaal { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "money")]
        [DisplayName("Price")]
        public decimal Prijs { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(250, ErrorMessage = "{0} only allows a max of 250 characters long.")]
        [DisplayName("Photo")]
        public string Foto { get; set; }
    }
}