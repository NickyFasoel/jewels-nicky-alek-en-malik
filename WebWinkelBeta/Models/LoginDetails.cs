﻿using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace WebWinkel.Models
{
    [Helpers.WrongDataAttribute()]
    public class LoginDetails
    {
        [Key]
        [Required(ErrorMessage = "You forgot {0}.")]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
		[DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "{0} has to be atleast 6 characters long.")]
		public string Password { get; set; }

        public int Attempts { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime LastAttempt { get; set; }
    }
}