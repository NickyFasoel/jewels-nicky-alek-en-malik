﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Sessies
    {
        [Key]
        public int SessieID { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "DateTime")]
        public DateTime Aangemaakt { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "bit")]
        public bool Valid { get; set; }

        public virtual LoginDetails LoginDetails { get; set; }
    }
}