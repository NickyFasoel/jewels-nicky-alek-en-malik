﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Postcodes
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [StringLength(4, ErrorMessage = "{0} only allows 4 characters.")]
        [Column(TypeName = "char")]
        [DataType("int")]
        public string Postcode { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [MaxLength(50, ErrorMessage = "{0} only allows a max of 50 characters long.")]
        public string Stad { get; set; }

        public string ConcatString
        {
            get
            {
                return $"{this.Stad} - {this.Postcode}";
            }
        }
    }
}