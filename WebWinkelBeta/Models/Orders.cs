﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Orders
    {
        [Key]
        public int ID { get; set; }

        public int Aantal { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BestelDatum { get; set; }
        
        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "money")]
        public decimal TotaalPrijs { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "bit")]
        public bool Betaald { get; set; }

        [Required(ErrorMessage = "You forgot {0}.")]
        [Column(TypeName = "bit")]
        public bool Retour { get; set; }

        public virtual Customers KlantID { get; set; }
    }
}