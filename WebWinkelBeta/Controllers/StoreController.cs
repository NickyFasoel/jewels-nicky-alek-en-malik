﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace WebWinkel.Controllers
{
    public class StoreController : Controller
    {
        private Helpers.Cookies cookieHelper = new Helpers.Cookies();
        private Helpers.DataAccess _dal = new Helpers.DataAccess();
        private HttpCookie cartCookie;

        private List<Models.OrderDetails> SetOrderDetails(Dictionary<string, string> tempDict)
        {
            List<Models.OrderDetails> orderDetails = new List<Models.OrderDetails>();

            foreach (KeyValuePair<string, string> item in tempDict)
            {
                orderDetails.Add(new Models.OrderDetails() { Aantal = int.Parse(item.Value), ProductID = _dal.ReadKnikker(int.Parse(item.Key)) });
            }

            return orderDetails;
        }

        private Models.Orders CreateOrder(int id)
        {
            return new Models.Orders() { BestelDatum = DateTime.Now, Betaald = false, Retour = false, KlantID = _dal.ReadCustomer(id) };
        }

        private bool PlaceOrder(Models.Customers customer, List<Models.OrderDetails> orderDetails)
        {
            Helpers.PlaceOrder place = new Helpers.PlaceOrder(this.CreateOrder(customer.ID), orderDetails);
            return place.ExecuteOrder();
        }

        public ActionResult Store(string searchBoxValue, string searchValue)
        {
            if (Request.Cookies["cartCookie"] != null)
            {
                cartCookie = Request.Cookies["cartCookie"];
                ViewBag.cartItems = cartCookie.Value;
            }

            if ((!String.IsNullOrEmpty(searchBoxValue)) && (searchBoxValue != "Search for..."))
            {
                if (searchValue == "Name")
                {
                    ViewBag.Lijst = _dal.SearchKnikkers(searchBoxValue, SearchEnum.Naam);
                }
                if (searchValue == "Color")
                {
                    ViewBag.Lijst = _dal.SearchKnikkers(searchBoxValue, SearchEnum.Kleur);
                }
            }
            return View(_dal.ReadAllKnikkers);
        }

        public ActionResult AddToCart(int id)
        {
            if (!string.IsNullOrEmpty(Request["input" + id]) && decimal.Parse(Request["input" + id]) > 0)
            {
                if (Request.Cookies["cartCookie"] != null)
                {
                    cartCookie = Request.Cookies["cartCookie"];
                    cartCookie.Value += id + " " + Request["input" + id] + "|";
                }
                else
                {
                    cartCookie = new HttpCookie("cartCookie", id.ToString() + " " + Request["input" + id] + "|");
                }
                Response.Cookies.Add(cartCookie);
                ViewBag.cartItems = cartCookie.Value;
            }
            else
            {
                ViewBag.cartItems = Request.Cookies["cartCookie"].Value;
            }

            return View("Store", _dal.ReadAllKnikkers);
        }

        public ActionResult RemoveFromCart(int? id)
        {
            cartCookie = Request.Cookies["cartCookie"];
            cookieHelper.RemoveId(cartCookie, id.ToString());
            Response.Cookies.Add(cartCookie);
            ViewBag.cartItems = cartCookie.Value;

            return View("Store", _dal.ReadAllKnikkers);
        }

        public ActionResult CheckoutCart(string cookie)
        {
            if (cookie == null || !cookieHelper.VerifyData(cookie))
            {
                ViewBag.CartMessage = "Please log in before ordering.";
            }
            else
            {
                if (Request.Cookies["cartCookie"].Value != "")
                {
                    cartCookie = Request.Cookies["cartCookie"];

                    if (this.PlaceOrder(cookieHelper.FindId(cookie), this.SetOrderDetails(Helpers.Cookies.CookieToDictionary(cartCookie.Value))))
                    {
                        cartCookie.Value = null;
                        Response.Cookies.Add(cartCookie);
                    }

                    ViewBag.CartMessage = "Your order has been placed!";
                }
                else
                {
                    ViewBag.CartMessage = "Please add an item before you checkout.";
                }
            }

            return View("Store", _dal.ReadAllKnikkers);
        }
    }
}