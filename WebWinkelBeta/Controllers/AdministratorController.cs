﻿using System.Net;
using System.Web.Mvc;
using WebWinkel.Models;

namespace WebWinkel.Controllers
{
    public class AdministratorController : Controller
    {
        Helpers.Cookies _cookies = new Helpers.Cookies();
        private Helpers.DataAccess _dal = new Helpers.DataAccess();
        private static string _koek;

        private bool Verify()
        {
            if (_cookies.User(_koek) == "Admin" && _cookies.VerifyData(_koek))
            {
                return true;
            }
            return false;
        }
        
        public ActionResult Admin(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                _koek = data;
            }
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View(_dal.ReadAllKnikkers);
        }

        public ActionResult Details(int? id)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Knikkers knikkers = _dal.ReadKnikker((int)id);
            if (knikkers == null)
            {
                return HttpNotFound();
            }
            return View(knikkers);
        }

        public ActionResult Create()
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Naam,Grootte,Kleur,Materiaal,Prijs,Foto")] Knikkers knikker)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (ModelState.IsValid && _dal.InsertKnikker(knikker))
            {
                return RedirectToAction("Admin");
            }

            return View(knikker);
        }

        public ActionResult Edit(int? id)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Knikkers knikker = _dal.ReadKnikker((int)id);
            if (knikker == null)
            {
                return HttpNotFound();
            }
            return View(knikker);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Naam,Grootte,Kleur,Materiaal,Prijs,Foto")] Knikkers knikker)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (ModelState.IsValid && _dal.UpdateKnikker(knikker))
            {
                return RedirectToAction("Admin");
            }
            return View(knikker);
        }

        public ActionResult Delete(int? id)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Knikkers knikkers = _dal.ReadKnikker((int)id);
            if (knikkers == null)
            {
                return HttpNotFound();
            }
            return View(knikkers);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.Verify())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (!_dal.DeleteKnikker(id))
            {
                return View();
            }
            return RedirectToAction("Admin");
        }
    }
}
