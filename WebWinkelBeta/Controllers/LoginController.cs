﻿using System.Web.Mvc;
using System.Net;

namespace WebWinkel.Controllers
{
    public class LoginController : Controller
    {
        private Helpers.DataAccess _dal = new Helpers.DataAccess();

        private bool LoggingIn(string username, string password)
        {
            HttpContext.Response.SetCookie(new Helpers.Cookies().LoginCookie(username));
            return true;
        }

        private Models.Customers Customer(Helpers.Register data)
        {
            Models.Customers customer = new Models.Customers();
            customer.Huisnummer = data.Huisnummer;
            customer.Familienaam = data.Familienaam;
            customer.Email = data.Email;
            customer.Land = data.Land;
            customer.Straat = data.Straat;
            customer.Voornaam = data.Voornaam;
            customer.Postcodes = _dal.ReadPostcode(data.PostcodeID);
            customer.LoginData = _dal.ReadLogin(data.UserName);

            return customer;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Username,Password")] Models.LoginDetails data)
        {
            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!ModelState.IsValid || !this.LoggingIn(data.UserName, data.Password))
            {
                return View();
            }

            return RedirectToAction("Home", "Home");
        }

        public ActionResult Register()
        {
            ViewBag.PostcodeID = new SelectList(_dal.ReadAllPostcodes, "ID", "ConcatString");

            return View("Registration");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "Voornaam,Familienaam,Straat,Huisnummer,Land,Email,UserName,Password,PostcodeID")] Helpers.Register data)
        {
            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.PostcodeID = new SelectList(_dal.ReadAllPostcodes, "ID", "ConcatString");

            if (!ModelState.IsValid)
            {
                return View("Registration", data);
            }

            if (!new Helpers.Login(data.UserName, data.Password).Create())
            {
                return View("Registration", data);
            }

            _dal.InsertCustomer(this.Customer(data));

            this.LoggingIn(data.UserName, data.Password);

            return RedirectToAction("Home", "Home");
        }
    }
}