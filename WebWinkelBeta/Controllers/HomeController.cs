﻿using System;
using System.Web;
using System.Web.Mvc;

namespace WebWinkel.Controllers
{
    public class HomeController : Controller
    {
        private Helpers.Cookies koeken = new Helpers.Cookies();

        private HttpCookie UserDataCookie()
        {
            try
            {
                return Request.Cookies["KnikkersStore"];
            }
            catch (NullReferenceException)
            {
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Home()
        {
            ViewBag.LoggedIn = koeken.Verify(Request.Cookies["KnikkersStore"]);
            return View();
        }

        public string CheckUser()
        {
            HttpCookie cookie = this.UserDataCookie();
            if (cookie == null)
            {
                return null;
            }
            return koeken.User(cookie.Value.ToString());
        }

        public bool Cookie()
        {
            HttpCookie cookie = this.UserDataCookie();
            if (cookie == null)
            {
                return false;
            }
            return koeken.Verify(cookie);
        }

        public ActionResult Logout(string cookie)
        {
            HttpContext.Response.SetCookie(new Helpers.Cookies().Logout(this.UserDataCookie()));
            return View("Home");
        }
    }
}