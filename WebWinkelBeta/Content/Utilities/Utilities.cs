﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebShop.Content.Utilities
{
	public static class Utilities
	{
		public static string IsActive(this HtmlHelper html, string controller, string action)
		{
			var routeData = html.ViewContext.RouteData;

			var routeAction = (string)routeData.Values["action"];
			var routeController = (string)routeData.Values["controller"];

			// both must match
			var returnActive = controller == routeController &&
							   action == routeAction;

			return returnActive ? "active" : "";
		}
    }
}